# WSGI server
Provides microservices in the companion-to-the-earth repositories that require web connectivity with a WSGI server.

## usage
Automatically launches a Cheroot server for Flask apps located in `server/framework/app/src/`. Apps are hosted at port 5000.

## image structure
<pre>
./
└── server/
    ├── wsgi/
    |   ├── requirements.txt
    |   └── app/
    |       ├── __init__.py
    |       ├── src/
    |       |   ├── __init__.py
    |       |   ├── server.py           #launches cheroot server
    |       |   ├── server.debug.py     #launches flask's build-in server for debugging
    |       |   └── ...
    |       ├── test_unit/
    |       |   └── ...
    |       └── test_integration/
    |           └── ...
    └── framework/
        └── app/
            ├── __init__.py
            ├── src/
            |   ├── __init__.py
            |   ├── app.py              #flask app initialiser; must include function get_app()
            |   └── ...
            ├── test_unit/
            |   └── ...
            └── test_integration/
                └── ...
</pre>

## used resources
- Cheroot: <https://cheroot.cherrypy.org/>
- Flask: <https://flask.palletsprojects.com/>
