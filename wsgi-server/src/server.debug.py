"""Debug server module that uses the default Flask WSGI server"""

from src.app import get_app

# get the Flask application from the app
app = get_app()

# start the server if this script was called directly
if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5000, debug=True)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)

