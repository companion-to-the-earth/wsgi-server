"""Server module that provides a Cheroot server to microservices"""

import signal
import sys
from cheroot.wsgi import Server
from src.app import get_app

# get the Flask application from the app
app = get_app()

# initialise the server
server = Server(('0.0.0.0', 5000), app)

def handle_sigterm(*_):
   server.stop()
   sys.exit()

# start the server if this script was called directly
if __name__ == '__main__':
   signal.signal(signal.SIGTERM, handle_sigterm)
   server.start()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)

